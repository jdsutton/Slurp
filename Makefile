PIP=python3.6 `which pip`
TARGET=slurp parsers dependency

install:
	$(PIP) install --upgrade .

test:
	python3 `which nosetests` $(TARGET) --nocapture

docs:
	mkdir -p documentation
	rm -f documentation/*.html
	pydoc -w ./ > documentation/log.txt
	mv *.html documentation
	rm *.pyc
