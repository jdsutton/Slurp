from itertools import chain
import re

class JavaScript:
    BUILT_IN_OBJECTS = set([
        'Object',
        'Function',
        'Boolean',
        'Symbol',
        'Error',
        'EvalError',
        'InternalError',
        'RangeError',
        'ReferenceError',
        'SyntaxError',
        'TypeError',
        'URIError',
        'Number',
        'Math',
        'Date',
        'String',
        'RegExp',
        'Array',
        'Int8Array',
        'Uint8Array',
        'Uint8ClampedArray',
        'Int16Array',
        'Uint16Array',
        'Int32Array',
        'Uint32Array',
        'Float32Array',
        'Float64Array',
        'Map',
        'Set',
        'WeakMap',
        'WeakSet',
        'ArrayBuffer',
        'SharedArrayBuffer',
        'Atomics',
        'DataView',
        'JSON',
        'Promise',
        'Generator',
        'GeneratorFunction',
        'AsyncFunction',
        'Reflect',
        'Proxy',
        'Intl',
        'WebAssembly',
        'arguments',
    ])
    RESERVED_WORDS = set([
        'abstract',
        'arguments',
        'await',
        'boolean',
        'break',
        'byte',
        'case',
        'catch',
        'char',
        'class',
        'const',
        'constructor',
        'continue',
        'debugger',
        'default',
        'delete',
        'do',
        'double',
        'else',
        'enum',
        'eval',
        'export',
        'extends',
        'false',
        'final',
        'finally',
        'float',
        'for',
        'function',
        'goto',
        'if',
        'implements',
        'import',
        'in',
        'instanceof',
        'int',
        'interface',
        'let',
        'long',
        'native',
        'new',
        'null',
        'of',
        'package',
        'private',
        'protected',
        'public',
        'return',
        'short',
        'static',
        'super',
        'switch',
        'synchronized',
        'this',
        'throw',
        'throws',
        'transient',
        'true',
        'try',
        'typeof',
        'var',
        'void',
        'volatile',
        'while',
        'with',
        'yield',
        'format',
    ])
    IDENTIFIER_PATTERN = '[_$a-zA-Z]+[_$a-zA-Z0-9]*'
    _FUNCTION_DECLARATION_PATTERN = re.compile('(function\s+|static\s+)?([_$a-zA-Z]+[_$a-zA-Z0-9]*)\s*\(')
    _CLASS_DECLARATION_PATTERN = re.compile('class\s+({})'.format(
        IDENTIFIER_PATTERN))
    _FUNCTION_PARAMETER_DECLARATION_PATTERN = re.compile('\(\s*([^)]+?)\s*\)'.format(
        IDENTIFIER_PATTERN, IDENTIFIER_PATTERN))
    _VALID_IDENTIFIER_CHARACTERS = set('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_$.[]()\'",')
    _VARIABLE_DECLARATION_PATTERN = re.compile('(var|let|const)\s+(([_$a-zA-Z]+[_$a-zA-Z0-9]*),?\s*)+(\s+|;|=)')

    @classmethod
    def isReserved(cls, s):
        return s in cls.RESERVED_WORDS or s in cls.BUILT_IN_OBJECTS

    @classmethod
    def getTopLevelName(cls, code, endIndex):
        '''
        Gets top-level identifier from subproperty.
        Eg: Object.foo.bar --> Object.
        '''

        startIndex = endIndex

        while startIndex > 0 and code[startIndex - 1] in cls._VALID_IDENTIFIER_CHARACTERS:
            startIndex -= 1

        while endIndex < len(code) - 1 and code[endIndex + 1] in cls._VALID_IDENTIFIER_CHARACTERS:
            endIndex += 1

        return code[startIndex:endIndex + 1].split('.')[0].split('[')[0].split('(')[0].split(')')[0]

    @classmethod
    def getDeclaredIdentifiers(cls, code):
        functions = map(lambda x: x.group(2),
            cls._FUNCTION_DECLARATION_PATTERN.finditer(code))
        classes = map(lambda x: x.group(1),
            cls._CLASS_DECLARATION_PATTERN.finditer(code))

        result = list(chain(functions, classes))

        parameters = map(lambda x: x.group(1),
            cls._FUNCTION_PARAMETER_DECLARATION_PATTERN.finditer(code))

        for params in parameters:
            params = params.split(',')

            for param in params:
                result.append(param.strip())

        variables = map(lambda x: x.group(),
            cls._VARIABLE_DECLARATION_PATTERN.finditer(code))

        for variable in variables:
            variable = re.sub(r'(var|let|const|in|of|;|\s+|=)', '', variable)
            variable = variable.split(',')

            for v in variable:
                result.append(v)

        def isValid(identifier):
            return identifier not in JavaScript.RESERVED_WORDS \
                and identifier not in JavaScript.BUILT_IN_OBJECTS

        result = filter(isValid, result)

        return set(result)