#! /usr/bin/env python3

from slurp.languages.JavaScript import JavaScript
import unittest

class test_JavaScript(unittest.TestCase):

    def test_getTopLevelName(self):
        s = '    a.b.c.d["e"].f.g'

        result = JavaScript.getTopLevelName(s, len(s) - 1)
        self.assertEqual('a', result)

        result = JavaScript.getTopLevelName(s, s.index('a'))
        self.assertEqual('a', result)

        s = '''
            function foo(a, b, c) {
                var bar = 'this is a string';

                return c;
            }
        '''

        result = JavaScript.getTopLevelName(s, s.index('foo'))
        self.assertEqual('foo', result)

        s = 'let x = data[key]'

        result = JavaScript.getTopLevelName(s, s.index('data'))
        self.assertEqual('data', result)

        s = 'new _v0().foo();'

        result = JavaScript.getTopLevelName(s, s.index('foo'))
        self.assertEqual('_v0', result)

        s = 'function foo(a,b,c){}'

        result = JavaScript.getTopLevelName(s, s.index('c', 10))
        self.assertEqual('foo', result)

        s = 'for (let key in data)'
        result = JavaScript.getTopLevelName(s, s.index('data'))
        self.assertEqual('data', result)

        s = 'var _v1=__slurp__st_0[1].format(blah);'
        result = JavaScript.getTopLevelName(s, s.index('blah'))
        self.assertEqual('__slurp__st_0', result)


    def test_getDeclaredIdentifiers(self):
        s = '''
            var x, y, z;

            var blah = 'format';

            class Foo {};

            function bar() {

            }

            let baz=100;

            class Bamboozle {
                constructor(flub, blub) {}
            }
        '''

        result = JavaScript.getDeclaredIdentifiers(s)
        
        self.assertTrue('x' in result)
        self.assertTrue('y' in result)
        self.assertTrue('z' in result)
        self.assertTrue('blah' in result)
        self.assertTrue('Foo' in result)
        self.assertTrue('bar' in result)
        self.assertTrue('baz' in result)
        self.assertTrue('Bamboozle' in result)
        self.assertTrue('flub' in result)
        self.assertTrue('blub' in result)

if __name__ == '__main__':
    unittest.main()