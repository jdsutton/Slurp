import logging
import signal

class Config:

    @staticmethod
    def init():
        def signalHandler(signal, frame):
            exit(0)

        signal.signal(signal.SIGINT, signalHandler)

        logging.basicConfig(filename='.slurplog', filemode='w', level=logging.DEBUG)
