from os import makedirs
from os.path import isfile, exists
from slurp.Config import Config
from slurp.SlurpItem import SlurpItem

import logging

def slurp(path, newPath, fileExtensions=dict()):
    '''
    Loads a file and writes it to the build stream.
    '''
    if isfile(path):
        extension = path.split('.')[-1]
        if extension in fileExtensions:
            contents = None

            with open(path, 'r', encoding='utf-8') as f:
                try:
                    contents = f.read()
                except UnicodeError:
                    logging.warning('UnicodeError reading ' + path)

            item = SlurpItem(path, newPath, contents)
        else:
            item = SlurpItem(path, newPath, None)
    else:
        if not exists(newPath):
            makedirs(newPath)

        item = SlurpItem(path, newPath, None)
    
    item.print()