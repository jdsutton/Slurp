import base64
import logging
from os import makedirs, remove
from os.path import abspath, isdir, isfile, dirname
from shutil import copyfile, SameFileError
import sys

class SlurpItem:
    def __init__(self, path, dest, contents):
        self.path = path
        self.dest = dest
        self.contents = contents

    def copy(self):
        return SlurpItem(self.path, self.dest, self.contents)

    def getOriginPath(self):
        return abspath(self.path)

    @staticmethod
    def _encode(s):
        return base64.b64encode(s.encode('utf-32')).decode()

    @staticmethod
    def _decode(s):
        return base64.b64decode(s).decode('utf-32')

    def print(self):
        if self.contents is not None:
            lines = self.contents.split('\n')

            output = '{}\n{}\n{}\n{}\n'.format(
                self.path,
                self.dest,
                len(lines),
                SlurpItem._encode(self.contents),
            )
        else:
            output = '{}\n{}\n{}\n'.format(self.path, self.dest, '0')

        # for ch in output:
        print(output, end='')

        sys.stdout.flush()

    def getSourceDirectory(self):
        return dirname(abspath(self.path))

    @staticmethod
    def fromPath(path):
        result = SlurpItem(None, None, None)

        result.path = path

        try:
            with open(path, 'r', encoding='utf-8') as f:
                result.contents = f.read()
        except UnicodeDecodeError:
            return None

        return result

    def isdir(self):
        return isdir(self.path)

    def isfile(self):
        return isfile(self.path)

    def write(self):
        if self.isdir():
            try:
                makedirs(self.dest)
            except FileExistsError:
                pass
        elif self.contents is not None:
            try:
                makedirs(dirname(self.dest))
            except FileExistsError:
                pass
            f = open(self.dest, 'w', encoding='utf-8')
            f.write(self.contents)
            f.close()
        else:
            try:
                copyfile(self.path, self.dest)
            except SameFileError:
                pass

    @classmethod
    def readIn(cls):
        linesRead = 0
        filePath = None
        destPath = None
        numLines = None
        contents = None

        filePath = input().strip()

        try:
            destPath = input().strip()
            numLines = int(input())

            if numLines > 0:
                # lines = []
                # for _ in range(numLines):
                #     lines.append(input())
                #     linesRead += 1

                try:
                    contents = cls._decode(input().strip())
                except:
                    logging.error('Failed to decode: ' + filePath)

                # contents = '\n'.join(lines)
            else:
                contents = None
        except EOFError as e:
            logging.warning(e)
            logging.warning('Source: {}'.format(filePath))
            logging.warning('Destination: {}'.format(destPath))
            logging.warning('Expected lines: {}'.format(numLines))
            logging.warning('Lines read: {}'.format(linesRead))
            logging.warning('Contents: {}'.format(contents))

            raise RuntimeError('Failed to read in file. Check .slurplog for errors')

        return SlurpItem(filePath, destPath, contents)