#! /usr/bin/env python3
'''
Renames references to changed files to update the browser cache.
Eg: ...src="styles.css" --> ...src="styles.css?v=sdafl2438ofonw"
'''

from argparse import ArgumentParser
from base64 import urlsafe_b64encode
from slurp.SlurpItem import SlurpItem
from hashlib import md5
import logging
from os import getcwd, mkdir, listdir, rename
from os.path import abspath, dirname, join, isfile, dirname, sep as pathSeparator
from re import finditer

logging.basicConfig(filename='.slurplog', filemode='w', level=logging.DEBUG)

FILE_PATH_PATTERN = '(src|href)=("|\')[\.a-zA-Z0-9_/]+\.[a-zA-Z]+("|\')'

def getHash(text):
    '''
    Returns a hash of a file's contents.
    '''

    return str(urlsafe_b64encode(md5(text.encode()).digest()).decode('utf-8'))

def parseArgs():
    '''
    Parse CLI arguments.
    '''

    parser = ArgumentParser()
    parser.add_argument('--fileExtensions',
        help='File extensions to rename, eg: css,html,js',
        type=str)

    return parser.parse_args()

def main():
    args = parseArgs()
    items = []
    newNames = dict()
    validFileExtensions = set(['html', 'htm', 'css'])

    if args.fileExtensions is not None:
        extensionsToRename = set(args.fileExtensions.split(','))
    else:
        extensionsToRename = set(['css'])

    while True:
        try:
            item = SlurpItem.readIn()
        except(EOFError):
            break

        if item.isfile():
            extension = item.path.split('.')[-1]
            if extension in extensionsToRename:
                fileHash = '?v=' + getHash(item.contents)

                sourcePath = item.path

                newNames[abspath(sourcePath)] = fileHash

        items.append(item)

    # Replace old file names in all files.
    for item in items:
        extension = item.path.split('.')[-1]

        if extension in validFileExtensions:
            possiblePaths = list(finditer(FILE_PATH_PATTERN, item.contents))

            for path in possiblePaths:
                path = path.group()
                path = path[path.index('=') + 2:-1]
                altPath = path

                if (path.startswith(pathSeparator)):
                    altPath = join(getcwd(), altPath[1:])
                else:
                    sourceDir = dirname(item.path)
                    altPath = join(sourceDir, altPath)

                altPath = abspath(altPath)

                if altPath in newNames:
                    fileHash = newNames[altPath]
                    item.contents = item.contents.replace(path, path + fileHash)

        item.write()

if __name__ == '__main__':
    main()