#! /usr/bin/env python3

from argparse import ArgumentParser
from slurp.build.Minifier import Minifier
from slurp.build.PathUtils import PathUtils
from slurp.Config import Config
from slurp.SlurpItem import SlurpItem
from os.path import abspath

Config.init()

def parseArgs():
    parser = ArgumentParser()
    parser.add_argument('--obfuscate', action="store_true")
    parser.add_argument('--keepComments', action="store_true")
    parser.add_argument('--ignore', help='Path patterns to ignore')
    parser.add_argument('--only', help='Only minify these paths')

    return parser.parse_args()

def main():
    args = parseArgs()

    while True:
        try:
            item = SlurpItem.readIn()
            
            if PathUtils.isValid(item, args.only, args.ignore):
                Minifier.currentFile = item.path
                item.contents = Minifier.minify(item.contents, args.obfuscate,
                    removeComments=(not args.keepComments))

            item.print()
        except(EOFError):
            break

if __name__ == '__main__':
    main()
