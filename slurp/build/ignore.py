#! /usr/bin/env python3

from argparse import ArgumentParser
from slurp.SlurpItem import SlurpItem
from os.path import sep

def parseArgs():
    parser = ArgumentParser()
    parser.add_argument('toIgnore', nargs='+', help='Path patterns to ignore')

    return parser.parse_args()

def main():
    args = parseArgs()
    toIgnore = set(args.toIgnore)

    while True:
        try:
            item = SlurpItem.readIn()
            ignoredDir = False

            for directory in item.path.split(sep):
                if directory in toIgnore:
                    ignoredDir = True
                    break

            if not ignoredDir:
                item.print()
        except(EOFError):
            break

if __name__ == '__main__':
    main()
