from slurp.build.StringUtils import StringUtils
from slurp.languages.JavaScript import JavaScript
import logging
import re

class Minifier:
    currentFile = 'No file'
    _varIndex = 0
    _stringTableIndex = 0
    _newNameByIdentifier = dict()
    _operators = '+/*|^%<>!&~-'
    _IDENTIFIER_REGEX = re.compile(JavaScript.IDENTIFIER_PATTERN)
    _LOOKBEHINDS = ['(?<!({}))'.format(w) for w in JavaScript.RESERVED_WORDS]
    _LOOKAHEADS = ['(?!{})'.format(w) for w in JavaScript.RESERVED_WORDS]
    _STRING_TABLE_FORMAT = '__slurp__st_{}'
    _TEMPLATE_PATTERN = re.compile('\$\{([_$a-zA-Z]+[_$a-zA-Z0-9]*)\}')
    _WHITESPACE_PATTERN = r''.join(_LOOKBEHINDS) + '\s' + ''.join(_LOOKAHEADS)
    _WHITESPACE_PATTERN2 = r'(;)\s+'

    @classmethod
    def reset(cls):
        cls._varIndex = 0
        cls._stringTableIndex = 0
        cls._newNameByIdentifier = dict()

    @staticmethod
    def _getFormatStringReplacement(s):
        '''
        Returns a replacement for a fomat string.
        Eg: `This is a ${blah} string` --> 'This is a {0} string', ['blah']
        '''

        identifiers = []
        match = Minifier._TEMPLATE_PATTERN.search(s)

        while match:
            startIndex = match.start()
            endIndex = match.end()
            identifier = match.group(1)

            s = s[:startIndex] + '{' + str(len(identifiers)) + '}' + s[endIndex:]
            identifiers.append(identifier)

            match = Minifier._TEMPLATE_PATTERN.search(s, startIndex + 1)

        return s[1:len(s) - 1], identifiers

    @staticmethod
    def _removeStrings(code):
        '''
        Strips all strings from a piece of code and returns them in a list.
        '''

        # TODO: Handle string chars inside comments when keeping comments.

        allStrings = []
        stringTableIndex = Minifier._stringTableIndex
        JavaScript.RESERVED_WORDS.add(
            Minifier._STRING_TABLE_FORMAT.format(Minifier._stringTableIndex))
        Minifier._stringTableIndex += 1

        i = StringUtils.findNextStr(code)
        while i:
            nextIndex = StringUtils.findNextStr(code, i + 1, code[i])

            if nextIndex is None:
                logging.warning('Unclosed string in file {}: {}'.format(
                    Minifier.currentFile, code[i-100:i+100]))
                i = StringUtils.findNextStr(code, i + 1)
                continue

            # If start of string, remove and replace.
            stringToReplace = code[i + 1:nextIndex]
            identifiers = None

            if code[i] == '`':
                # Handle format strings.
                stringToReplace, identifiers = Minifier._getFormatStringReplacement(code[i:nextIndex + 1])

            # Add to string table.
            allStrings.append(stringToReplace)

            # Replace with index into string table.
            replacement = '{}[{}]'.format(
                Minifier._STRING_TABLE_FORMAT.format(stringTableIndex),
                len(allStrings) - 1);
            
            # Format in variable values if format string.
            if identifiers:
                replacement = replacement + '.format({})'.format(','.join(identifiers))

            # Patch in replacement.
            code = code[:i] + replacement + code[nextIndex + 1:]

            i = StringUtils.findNextStr(code, i + 1)

        return code, allStrings

    @staticmethod
    def _replaceIdentifiers(code):
        '''
        Replaces all identifiers with shortened names.
        '''

        # Only replace identifiers with known definitions.
        knownDeclarations = JavaScript.getDeclaredIdentifiers(code)
        st = Minifier._STRING_TABLE_FORMAT.format('')

        # TODO: Handle identifiers like this.______

        match = Minifier._IDENTIFIER_REGEX.search(code)

        while match:
            group = 0
            identifier = match.group(group)
            startPos = match.start(group)
            endPos = match.end(group)

            if identifier.startswith(st) or JavaScript.isReserved(identifier):
                match = Minifier._IDENTIFIER_REGEX.search(code, endPos + 1)
                continue

            # TODO: Ignore identifiers which are raw object properties.

            # Ignore properties of built-ins.
            topLevelName = JavaScript.getTopLevelName(code, startPos)

            if topLevelName in knownDeclarations or topLevelName.startswith(st) \
            or topLevelName == 'constructor':
                if identifier in Minifier._newNameByIdentifier:
                    newName = Minifier._newNameByIdentifier[identifier]
                else:
                    newName = '_v{}'.format(Minifier._varIndex)
                    Minifier._varIndex += 1
                    Minifier._newNameByIdentifier[identifier] = newName
                    knownDeclarations.add(newName)

                code = code[:startPos] + newName + code[endPos:]

                endPos = startPos + len(newName)

            match = Minifier._IDENTIFIER_REGEX.search(code, endPos + 1)

        return code

    @staticmethod
    def _removeComments(code):

        # Handle line comments.
        code = StringUtils.stripOut(code, '//', '\n')

        # Handle block comments.
        code = StringUtils.stripOut(code, '/*', '*/')

        return code

    @staticmethod
    def _removeWhitespace(code, removeComments):
        '''
        TODO.
        '''

        lines = code.split('\n')

        for i in range(len(lines)):
            lines[i] = re.sub(Minifier._WHITESPACE_PATTERN, '', lines[i])
            lines[i] = re.sub(Minifier._WHITESPACE_PATTERN2, r'\1', lines[i]).strip(' ')

            if not removeComments:
                match = StringUtils.findNextStr(lines[i], 0, '//')

                if match is not None:
                    lines[i] = lines[i] + '\n'

        return ''.join(lines)

    @staticmethod
    def minify(code, obfuscate=False, *, removeComments=True):
        '''
        Minifies code.
        '''

        if removeComments:
            code = Minifier._removeComments(code)

        # Move all strings into string table.
        if obfuscate:
            stringTableIndex = Minifier._stringTableIndex
            code, allStrings = Minifier._removeStrings(code)

        # Remove whitespace.
        code = Minifier._removeWhitespace(code, removeComments)

        # Replace all identifiers with new identifier.
        if obfuscate:
            code = Minifier._replaceIdentifiers(code)

            code = 'var {}={};{}'.format(
                Minifier._STRING_TABLE_FORMAT.format(stringTableIndex),
                str(allStrings), code)

        return code