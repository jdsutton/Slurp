#! /usr/bin/env python3
'''
    Creates a manifest for each directory's contents.
'''

from os import mkdir, listdir
from os.path import join
from slurp.SlurpItem import SlurpItem

MANIFEST_NAME = 'manifest'

def buildManifest(path):
    '''
        Builds a manifest in the given directory.
    '''
    subpaths = listdir(path)
    manifestPath = join(path, MANIFEST_NAME)
    f = open(manifestPath,'w')
    for path in subpaths:
        if path.endswith(MANIFEST_NAME):
            continue
        if path is not subpaths[-1]:
            path = path + '\n'
        
        f.write(path)
    f.close()

    return manifestPath

def main():
    while True:
        try:
            item = SlurpItem.readIn()
        except(EOFError):
            break

        if item.isdir():
            buildManifest(item.dest)
          
        item.print()

if __name__ == '__main__':
    main()