#! /usr/bin/env python3

'''
'''

from argparse import ArgumentParser
from glob import iglob
from os.path import dirname, expanduser, isdir, join, realpath
from os import makedirs

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

from slurp.SlurpItem import SlurpItem
from slurp.slurp import slurp
import sys
import time

def parseArgs():
    '''
    Parses CLI arguments.
    '''
    parser = ArgumentParser()
    parser.add_argument('--sourceFilePaths', type=str)
    parser.add_argument('--destinationFilePaths', type=str)
    parser.add_argument('--webdriverLocation', type=str)
    parser.add_argument('--flagName', type=str, default='_RENDERED_STATIC')
    parser.add_argument('--timeout', type=int, default=1)

    return parser.parse_args()

class PyCrawler():

    class TimeoutError(RuntimeError):
        pass

    def __init__(self, driverLocation):
        options = webdriver.ChromeOptions()

        options.add_argument('headless')
        
        self._driver = webdriver.Chrome(driverLocation, chrome_options=options)

    def close(self):
        self._driver.close()

    def navigateTo(self, uri):
        self._driver.get(uri)

        return self

    def getHTML(self):
        return self._driver.find_element_by_tag_name('html').get_attribute('outerHTML')

    def runJS(self, js):
        return self._driver.execute_script(js)

    def waitForJS(self, js, timeoutSeconds=1):
        from datetime import datetime, timedelta

        timeout = timedelta(seconds=timeoutSeconds)
        start = datetime.now()
        result = self.runJS(js)

        while not result and (datetime.now() - start) < timeout:
            result = self.runJS(js)

        if not result:
            raise PyCrawler.TimeoutError()

        return self

    def injectJS(self, s):
        s = '<script id="_PyCrawer_Injected">{}</script>'.format(s)

        self.runJS('document.body.innerHTML=\'{}\'+document.body.innerHTML;'.format(s))

        return self

    def waitForElement(self, elementId, timeout=1):
        WebDriverWait(self._driver, timeout).until(EC.presence_of_element_located((By.ID, elementId)))

        return self

def staticify(sourcePaths, destPaths, webdriverLocation, flagName, *, timeout=1):
    crawler = PyCrawler(realpath(expanduser(webdriverLocation)))

    for i in range(len(sourcePaths)):
        sourcePath = 'file://' + realpath(sourcePaths[i])
        destPath = destPaths[i]
        destDir = dirname(destPath)

        crawler.navigateTo(sourcePath)
        time.sleep(timeout)
        crawler.injectJS('{}=true;'.format(flagName)) \
            .waitForElement('_PyCrawer_Injected')

        html = '<!DOCTYPE html>' + crawler.getHTML().encode('ascii', 'xmlcharrefreplace').decode()

        if not isdir(destDir):
            makedirs(destDir)

        with open(destPath, 'w') as f:
            f.write(html)

    crawler.close()

def main():
    args = parseArgs()

    sourcePaths = args.sourceFilePaths.split(',')
    destPaths = args.destinationFilePaths.split(',')

    staticify(
        sourcePaths, 
        destPaths, 
        args.webdriverLocation, 
        args.flagName,
        timeout=args.timeout,
    )
    
if __name__ == '__main__':
    main()
