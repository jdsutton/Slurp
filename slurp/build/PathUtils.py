from os.path import abspath

class PathUtils:

    @staticmethod
    def isValid(item, only, ignore):
        itemPath = item.getOriginPath()

        if only:
            only = list(map(abspath, only.split(',')))

        if ignore:
            ignore = ignore.split(',')

        if not item.path.endswith('.js'):
            return False

        if only:
            valid = False

            for path in only:
                if itemPath.startswith(path):
                    valid = True
                    break

            if not valid:
                return False

        if ignore:
            for path in ignore:
                if path in itemPath or itemPath.startswith(abspath(path)):
                    return False

        return True