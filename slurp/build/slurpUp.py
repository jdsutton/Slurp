#! /usr/bin/env python3

'''
slurp.py
This is a Slurp build module. This module handles the loading of project files into the build stream.
Build scripts will often being with this module.
'''

from argparse import ArgumentParser
from slurp.Config import *
from glob import iglob
import logging
from os.path import sep as pathSeparator, join
from os import walk
from slurp.SlurpItem import SlurpItem
from slurp.slurp import slurp
import sys

def parseArgs():
    '''
    Parses CLI arguments.
    '''
    parser = ArgumentParser()
    parser.add_argument('sourcePath', type=str, help='The source code directory.')
    parser.add_argument('destinationPath', type=str, help='The output directory.')
    parser.add_argument('--fileExtensions', type=str)
    parser.add_argument('--ignore', type=list, default=[], help='Path patterns to ignore')

    return parser.parse_args()

def main():
    '''
    Loads files from the specified source directory and writes them to the build stream.
    '''
    args = parseArgs()
    toIgnore = set(args.ignore)

    if args.fileExtensions is not None:
        fileExtensions = set(args.fileExtensions.split(','))
    else:
        fileExtensions = set(['html', 'htm', 'js', 'css'])

    sourcePaths = args.sourcePath.split(',')
    destPaths = args.destinationPath.split(',')

    for i in range(len(sourcePaths)):
        sourcePath = sourcePaths[i]
        destPath = destPaths[i % len(destPaths)]

        for path, folders, fileNames in walk(sourcePath, followlinks=True):

            for directory in path.split(pathSeparator):
                if directory in toIgnore:
                    continue

            # Slurp folders
            for folder in folders:
                fullPath = join(path, folder)
                newPath = fullPath.replace(sourcePath, destPath)
                slurp(fullPath, newPath, fileExtensions)

            # Slurp files
            for fileName in fileNames:
                fullPath = join(path, fileName)
                newPath = fullPath.replace(sourcePath, destPath, 1)
                try:
                    slurp(fullPath, newPath, fileExtensions)
                except Exception as e:
                    logging.warning('Could not slurp path: {}'.format(fullPath))
                    logging.warning(e)

    sys.stdout.flush()
            
if __name__ == '__main__':
    main()
