from slurp.languages.JavaScript import JavaScript

class StringUtils:

    @staticmethod
    def _charIsUnescaped(code, i):
        count = 0

        while i > 0 and code[i - 1] == '\\':
            count += 1
            i -= 1

        return count % 2 == 0

    @staticmethod
    def _matches(a, b, i):
        if a is None:
            return False

        for j in range(len(a)):
            index = i + j

            if index >= len(b):
                return False

            if b[index] != a[j]:
                return False

        return True

    @staticmethod
    def findNextStr(code, i=0, match=None, *, chars=set(['\'', '"', '`'])):
        escapableChars = set(['/', '\'', '"', '`'])

        while i < len(code):
            if StringUtils._matches(match, code, i) or (match is None and code[i] in chars):
                unescaped = code[i] not in escapableChars

                if unescaped or StringUtils._charIsUnescaped(code, i):
                    return i

            i += 1

        return None

    @staticmethod
    def stripOut(code, startStr, endStr):

        match = StringUtils.findNextStr(code, 0, startStr)

        while match is not None:
            end = StringUtils.findNextStr(code, match + len(startStr),
                endStr)

            if end is None:
                # End of file.
                code = code[:match]
                break
            else:
                code = code[:match] + code[end + len(endStr):]

            match = StringUtils.findNextStr(code, match, startStr)

        return code