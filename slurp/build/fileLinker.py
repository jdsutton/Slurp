#! /usr/bin/env python3

'''
Looks for strings like [[exampleFile.html]] and replaces them with the contents of the specified file.
'''

from argparse import ArgumentParser
from slurp.SlurpItem import SlurpItem
from os.path import abspath, getmtime
import logging
from parsers.MetaImportPathParser import MetaImportPathParser
from shutil import copyfile
from sys import argv, exit

def parseArgs():
    '''
    Parses CLI arguments.
    '''

    parser = ArgumentParser()
    parser.add_argument('--fileExtensions', type=str)
    parser.add_argument('--maxDepth', type=int,
        help='Maximum dependency depth to follow before giving up.')
    parser.add_argument('--stopOnLoop', type=bool,
        help='Whether to exit when maximum dependency depth is exceeded.')

    return parser.parse_args()

def main():
    args = parseArgs()

    if args.maxDepth is None:
        args.maxDepth = 10

    if args.fileExtensions is not None:
        fileExtensions = set(args.fileExtensions.split(','))
    else:
        fileExtensions = set(['html', 'htm'])

    while True:
        try:
            item = SlurpItem.readIn()
        except(EOFError):
            break

        if item.isfile():
            extension = item.path.split('.')[-1]

            if extension in fileExtensions:
                filesImported = set()

                MetaImportPathParser.handleItem(item, filesImported)

        item.print()

if __name__ == '__main__':
    main()
