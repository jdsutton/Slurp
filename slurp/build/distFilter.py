#! /usr/bin/env python3
'''
    Filters out files with 'nodist' in the first line
'''

from os import remove
from slurp.SlurpItem import SlurpItem

def main():
    validExtensions = set()
    validExtensions.add('html')
    validExtensions.add('htm')
    validExtensions.add('css')
    validExtensions.add('js')

    while True:
        try:
            item = SlurpItem.readIn()
        except(EOFError):
            break

        extension = item.path.split('.')[-1]
        if item.isfile() and extension in validExtensions:
            line = item.contents.split('\n')[0] # TODO: Make this more efficient
            if not 'nodist' in line:
                item.print()
        else:
            item.print()

if __name__ == '__main__':
    main()
