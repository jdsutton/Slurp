#! /usr/bin/env python3

from slurp.build.StringUtils import StringUtils
import unittest

class test_StringUtils(unittest.TestCase):

    def test_findNextStr(self):
        s = '// \n'

        result = StringUtils.findNextStr(s, 0, '//')
        self.assertEqual(0, result)

        result = StringUtils.findNextStr(s, 0, 'bar')
        self.assertEqual(None, result)

        result = StringUtils.findNextStr(s, 0, '\n')
        self.assertEqual(3, result)

    def test_stripOut(self):
        s = '12345 abc remove me! xyz 67890'

        result = StringUtils.stripOut(s, 'abc', 'xyz')

        self.assertEqual('12345  67890', result)

    def test_stripOut_lineComment(self):
        s = '''// Remove me lol'''

        result = StringUtils.stripOut(s, '//', '\n')

        self.assertEqual('', result)

if __name__ == '__main__':
    unittest.main()