#! /usr/bin/env python3

from slurp.build.Minifier import Minifier
import unittest

class test_Minifier(unittest.TestCase):

    def setUp(self):
        Minifier.reset()

    def test_minify(self):
        code = '''
            /**
             * @public
             */
            function foo(a, b, c) {
                // return c;
                return c;
            }
        '''

        result = Minifier.minify(code)

        self.assertEqual('function foo(a,b,c){return c;}', result)

    def test_minify_keep_comments(self):
        code = '''
            /**
             * @public
             */
            function foo(a, b, c) {
                // return c;
                return c;
            }
        '''

        result = Minifier.minify(code, removeComments=False)

        self.assertEqual('''/***@public*/function foo(a,b,c){// return c;
return c;}''', result)

    def test_obfuscate(self):
        code = '''
            function foo(a, b, c) {
                var bar = 'this is a string';

                return c;
            }
        '''

        result = Minifier.minify(code, True)
        expected = 'var __slurp__st_0=[\'this is a string\'];function _v0(_v1,_v2,_v3){var _v4=__slurp__st_0[0];return _v3;}'
        
        self.assertEqual(expected, result)

    def test_obfuscate_for_loop(self):
        code = '''
            const data = {};

            for (let key in data) {
                let datum = data[key];
            }
        '''

        result = Minifier.minify(code, True)
        expected = 'var __slurp__st_0=[];const _v0={};for (let _v1 in _v0){let _v2=_v0[_v1];}'
        
        self.assertEqual(expected, result)

    def test_obfuscate_function_call(self):
        code = '''
            function foo(a, b, c){ };

            const a = 1;
            const b = 2;
            const c = 3;
            foo(a, b, c);
        '''

        result = Minifier.minify(code, True)
        expected = 'var __slurp__st_0=[];function _v0(_v1,_v2,_v3){};const _v1=1;const _v2=2;const _v3=3;_v0(_v1,_v2,_v3);'
        
        self.assertEqual(expected, result)

    def test_obfuscate_class(self):
        code = '''
            class Blah {
                constructor(flub, blub) {

                }

                foo() {
                
                }

                static bar(a, b, c) {

                }
            }
        '''

        result = Minifier.minify(code, True)
        expected = 'var __slurp__st_0=[];class _v0{constructor(_v1,_v2){}_v3(){}static _v4(_v5,_v6,_v7){}}'
        
        self.assertEqual(expected, result)

    def test_obfuscate_method_call(self):
        code = '''
            class Blah {
                foo() {

                }

                static bar() {

                }
            }

            new Blah().foo();
            Blah.bar();
        '''

        result = Minifier.minify(code, True)
        expected = 'var __slurp__st_0=[];class _v0{_v1(){}static _v2(){}}new _v0()._v1();_v0._v2();'
        
        self.assertEqual(expected, result)

    # TODO: Test this._____

    def test_obfuscate_escaped_string_char(self):
        code = '''
            const x = '\\'';
            let y = '\\"';
        '''

        result = Minifier.minify(code, True)
        expected = '''var __slurp__st_0=["\\\\'", '\\\\"'];const _v0=__slurp__st_0[0];let _v1=__slurp__st_0[1];'''

        self.assertEqual(expected, result)

    def test_obfuscate_format_string(self):
        code = '''
            var blah = 'format';
            var bar = `This is a ${blah} string`;
        '''

        result = Minifier.minify(code, True)
        expected = 'var __slurp__st_0=[\'format\', \'This is a {0} string\'];var _v0=__slurp__st_0[0];var _v1=__slurp__st_0[1].format(_v0);'

        self.assertEqual(expected, result)

    # def test_obfuscate_preserve_unknowns(self):
    #     code = '''
    #         var assign = Object.assign({}, {a: 1, b: 2, c: 3, assign: 4});

    #         Object.assign = 10;
    #     '''
    #     expected = 'var __slurp__st_0=[];var _v0=Object.assign({},{a:1,b:2,c:3,x:4});Object.assign=10;'

    #     result = Minifier.minify(code, True)

    #     self.assertEqual(expected, result)

    # def test_obfuscate_handle_builtin_instance_methods(self):
    #     code = '''
    #         var a = (9).toFixed(2);
    #         var b = 'a b c d'.split(' ');
    #         var c = b.format(1);
    #         var d = 10.123456567;
    #         var e = d.toFixed(5);
    #     '''

    #     result = Minifier.minify(code, True)
    #     expected = ''
        
    #     self.assertEqual(expected, result)

if __name__ == '__main__':
    unittest.main()