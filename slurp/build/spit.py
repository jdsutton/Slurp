#! /usr/bin/env python3

from argparse import ArgumentParser
import logging
from slurp.SlurpItem import SlurpItem

logging.basicConfig(filename='.slurplog', filemode='w', level=logging.DEBUG)

def parseArgs():
    '''
        Parses CLI arguments.
    '''
    parser = ArgumentParser()
    parser.add_argument('--fileExtensions', type=str)
    parser.add_argument('--filePatterns', type=str)

    return parser.parse_args()


def main():
    args = parseArgs()

    logging.info('PATTERNS: {}'.format(args.filePatterns))

    fileExtensions = None
    if args.fileExtensions is not None:
        fileExtensions = set(args.fileExtensions.split(','))

    while True:
        try:
            item = SlurpItem.readIn()
            extension = item.dest.split('.')[-1]
            if fileExtensions is None or extension in fileExtensions:
                item.write()
            else:
                item.print()
        except(EOFError):
            break

if __name__ == '__main__':
    main()
        