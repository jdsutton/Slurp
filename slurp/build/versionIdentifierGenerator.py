#! /usr/bin/env python3

'''
Slurp module to replace all instances of [["versionIdentifier"]] with a version identifier.
'''

from argparse import ArgumentParser
import datetime
from envVars import fillInValues
from slurp.SlurpItem import SlurpItem

def parseArgs():
    '''
        Parses CLI arguments.
    '''
    parser = ArgumentParser()
    parser.add_argument('--fileExtensions', type=str)

    return parser.parse_args()

def main():
    args = parseArgs()
    
    identifier = datetime.datetime.now().strftime('%d-%m-%y')

    if args.fileExtensions is not None:
        fileExtensions = set(args.fileExtensions.split(','))
    else:
        fileExtensions = set(['html'])

    while True:
        try:
            item = SlurpItem.readIn()
        except(EOFError):
            break

        extension = item.path.split('.')[-1]
        if item.isfile() and extension in fileExtensions:
            item.contents = fillInValues(item.contents, {
                'versionIdentifier': identifier
            })

        item.print()

if __name__ == '__main__':
    main()
