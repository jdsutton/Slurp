#! /usr/bin/env python3

'''
    Usage: ... | wrap.py "command options [file|dir|file_or_dir]" ...
    Reads paths from STDIN, fills in the given command, and runs it if valid.
    Eg: ... | wrap.py "echo [file]" will run "echo fileFromStdin.something"

'''

from argparse import ArgumentParser
import subprocess
from os.path import isfile
from slurp.build.PathUtils import PathUtils
from slurp.SlurpItem import SlurpItem
from tempfile import NamedTemporaryFile

FILE_MARKER = '[file]'
DIR_MARKER = '[dir]'
FILE_OR_DIR_MARKER = '[file_or_dir]'

def replaceAll(s, substr, replacement):
    while substr in s:
        s = s.replace(substr, replacement)

    return s

def compileCmd(cmd, path):
    if isfile(path):
        cmd = replaceAll(cmd, FILE_MARKER, path)
        cmd = replaceAll(cmd, FILE_OR_DIR_MARKER, path)
    else:
        cmd = replaceAll(cmd, DIR_MARKER, path)
        cmd = replaceAll(cmd, FILE_OR_DIR_MARKER, path)

    if FILE_MARKER in cmd or DIR_MARKER in cmd or FILE_OR_DIR_MARKER in cmd:
        return None

    return cmd

def parseArgs():
    '''
        Parses CLI arguments.
    '''
    parser = ArgumentParser()
    parser.add_argument('cmd', type=str)
    parser.add_argument('--fileExtensions', type=str)
    parser.add_argument('--ignoreExitCode', type=bool)
    parser.add_argument('--noPrint', type=bool)
    parser.add_argument('--ignore', help='Path patterns to ignore')
    parser.add_argument('--only', help='Only minify these paths')


    args = parser.parse_args()

    if args.fileExtensions is not None:
        args.fileExtensions = set(args.fileExtensions.split(','))
    else:
        args.fileExtensions = set(['html', 'htm'])

    return args

def compileAndRun(cmd, path, ignoreExitCode, item):
    compiledCmd = compileCmd(cmd, path)

    if compiledCmd is not None:
        result = subprocess.check_output(compiledCmd.split())

def main():
    args = parseArgs()

    while True:
        try:
            item = SlurpItem.readIn()
        except(EOFError):
            break

        extension = item.path.split('.')[-1]

        if PathUtils.isValid(item, args.only, args.ignore) and extension in args.fileExtensions:
            f = NamedTemporaryFile(mode='w', delete=False, suffix='.'+extension)
            path = f.name
            f.write(item.contents)
            f.close()
            compileAndRun(args.cmd, path, args.ignoreExitCode, item)
            f = open(path, 'r')
            item.contents = f.read()
            f.close()

        if not args.noPrint:
            item.print()

if __name__ == '__main__':
    main()
