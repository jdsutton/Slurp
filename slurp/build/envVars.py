#! /usr/bin/env python3

'''
    Looks for strings like [["key"]] and replaces them with a value from the specified file.
    Useful for setting different values in different environments, eg: app IDs.
    Usage: ... | envVars.py path/to/myDictionary.json [environmentName] | ...
'''

from os import listdir, makedirs
from os.path import sep as pathSeparator, getmtime, join, split, isfile, exists, dirname
from re import compile as compilePattern
from shutil import copyfile
from sys import argv, exit
from argparse import ArgumentParser
import logging
from slurp.SlurpItem import SlurpItem
import json

from slurp.Config import Config

Config.init()

class EnvVarParser:
    TEMPLATE_PATTERN = compilePattern(r'\[\["[a-zA-Z0-9_/]+"\]\]')

def fillInValues(text, valuesDict):
    '''
    Fills values into a file.
    '''
    
    result = EnvVarParser.TEMPLATE_PATTERN.search(text)
    pos = 0

    while result is not None:
        key = result.group(0)[3:-3]
        if not key in valuesDict:
            value = 'Key not found: ' + key
        else:
            value = valuesDict[key]
        text = text[:result.start()] + value + text[result.end():]
        result = EnvVarParser.TEMPLATE_PATTERN.search(text, result.start() + 1)
    
    return text

def parseArgs():
    '''
    Parses CLI arguments.
    '''
    parser = ArgumentParser()
    parser.add_argument('jsonFilePath', type=str)
    parser.add_argument('env', type=str, nargs='?', default=None)
    parser.add_argument('--fileExtensions', type=str)
    parser.add_argument('--injectversion', action='store_true')

    return parser.parse_args()

def main():
    args = parseArgs()
    f = open(args.jsonFilePath)
    valuesDict = json.load(f)
    f.close()

    if args.env is not None:
        valuesDict = valuesDict[args.env]

    if args.injectversion:
        import datetime
        valuesDict['versionIdentifier'] = datetime.datetime.now().strftime('%d-%m-%y:%H:%M.%S')

    if args.fileExtensions is not None:
        fileExtensions = set(args.fileExtensions.split(','))
    else:
        fileExtensions = set(['js', 'html', 'css'])

    while True:
        try:
            item = SlurpItem.readIn()
        except(EOFError):
            break

        extension = item.path.split('.')[-1]
        if item.isfile() and extension in fileExtensions:
            item.contents = fillInValues(item.contents, valuesDict)

        item.print()

if __name__ == '__main__':
    main()
