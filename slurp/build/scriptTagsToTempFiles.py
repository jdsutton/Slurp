#! /usr/bin/env python3

'''
'''

from argparse import ArgumentParser
from glob import iglob
from os import walk, makedirs
from os.path import sep as pathSeparator, join
import sys
from parsers.ScriptTagIterator import ScriptTagIterator
from slurp.SlurpItem import SlurpItem

def parseArgs():
    '''
    Parses CLI arguments.
    '''
    parser = ArgumentParser()
    parser.add_argument('sourcePath', type=str, help='The source code directory.')
    parser.add_argument('destinationPath', type=str, help='The output directory.')
    parser.add_argument('--ignore', type=list, default=[], help='Path patterns to ignore')

    return parser.parse_args()

def main():
    '''
    Loads files from the specified source directory and writes them to the build stream.
    '''
    args = parseArgs()
    toIgnore = set(args.ignore)

    fileExtensions = set(['html', 'htm'])

    sourcePaths = args.sourcePath.split(',')
    destPath = args.destinationPath
    i = 0

    for sourcePath in sourcePaths:
        for path, folders, fileNames in walk(sourcePath, followlinks=True):

            for directory in path.split(pathSeparator):
                if directory in toIgnore:
                    continue

            # Slurp files
            for fileName in fileNames:

                fullPath = join(path, fileName)
                item = SlurpItem.fromPath(fullPath)

                if item is None:
                    continue

                iterator = ScriptTagIterator(item.contents)

                for script in iterator:

                    tempPath = join(destPath, '_temp_{}.js'.format(i))
                    i += 1

                    # Write script contents to destination path.
                    with open(tempPath, 'w') as f:
                        f.write(script.contents)

                    # Replace script contents with reference to temp file.
                    newValue = '\'[[{}]]\''.format(join('/', tempPath))

                    iterator.replaceScript(script, newValue)

                # Write file with scripts stripped out.
                with open(fullPath, 'w') as f:
                    f.write(iterator.contents)
            
if __name__ == '__main__':
    main()
