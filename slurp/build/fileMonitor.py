#!/usr/bin/env python3
'''
    Watches for changes in a directory
    Runs the given command when a change occurs
    Usage: python3 fileMonitor.py dir --destinationPath=$(DIST)
'''

from argparse import ArgumentParser
from dependency.DependencyAnalyzer import DependencyAnalyzer
from time import sleep
from glob import iglob
import logging
from os import remove
from os.path import abspath, realpath, getmtime, sep as pathSeparator, join, isfile
import subprocess
import sys
from slurp.SlurpItem import SlurpItem
from slurp.slurp import slurp
import traceback

DELAY_SECONDS = 1

from slurp.Config import Config

class FileMonitor:
    def __init__(self, path, ignore=[]):
        self._dir = path
        self._callback = None
        self._fileModificationTimes = dict()
        self._ignore = set(ignore)

        logging.info('Ignoring folders: {}'.format(self._ignore))

    def onChange(self, callback):
        self._callback = callback

    def getFileModificationTimes(self):
        '''
        Returns a dict mapping file paths to modification times.
        '''
        self._fileModificationTimes = dict()

        for file in self.getFilesInDir():      
            try:            
                self._fileModificationTimes[file] = getmtime(file)
            except:
                # File deleted.
                pass

        return self._fileModificationTimes

    def getFilesInDir(self):   
        '''
        Recursively returns all files under the monitor's source directories.
        '''  

        files = []

        for path in self._dir:
            files.append(path)

            for file in iglob(join(path, '**', '*'), recursive=True):
                ignore = False

                for folder in file.split(pathSeparator):
                    if folder in self._ignore:
                        ignore = True
                        break     

                if ignore:
                    continue
                
                files.append(file)
              
        return files

    def _onchange(self, file, maxDepth=15):
        if maxDepth < 1:
            raise RuntimeError('Possible dependency loop detected')
        
        for child in DependencyAnalyzer.getFilesDependingOn(self.getFilesInDir(), file):
            self._onchange(str(child), maxDepth - 1)

        self._callback(file)

        sys.stdout.flush()

    def poll(self):
        files = self.getFilesInDir()
        logging.info('Poll found {} files.'.format(len(files)))
        filesCreated = 0
        filesDeleted = 0
        filesChanged = 0

        # Check for deleted files.
        fileSet = set(files)
        for file in self._fileModificationTimes:
            if not file in fileSet:
                logging.info('File deleted: {}.'.format(file))
                filesDeleted += 1

        # Check for new and modified files
        for file in files:
            ignore = False

            for folder in file.split(pathSeparator):
                if folder in self._ignore:
                    ignore = True
                    break

            if ignore:
                continue

            try:            
                lastUpdatedTime = getmtime(file)
            except:
                # File deleted.
                continue

            if not file in self._fileModificationTimes:
                logging.info('File created: ' + file)
                filesCreated += 1
                self._callback(file)
            elif lastUpdatedTime > self._fileModificationTimes[file]:
                logging.info('File updated: {}. Time was: {}, now is: {}.'.format(file,
                    self._fileModificationTimes[file], lastUpdatedTime))
                filesChanged += 1
                self._onchange(file)

        logging.info('{} files created.'.format(filesCreated))
        logging.info('{} files deleted.'.format(filesDeleted))
        logging.info('{} files changed.'.format(filesChanged))

        self.getFileModificationTimes()

def parseArgs():
    '''
    Parses CLI arguments.
    '''
    parser = ArgumentParser()
    parser.add_argument('sourcePath', type=str, help='The source code directories.')
    parser.add_argument('--destinationPath', type=str, help='(Optional) The output directories.')
    parser.add_argument('--command', type=str, help='(Optional) The command to run when files have changed.')
    parser.add_argument('--fileExtensions', type=str, help='(Optional) File extensions of files to build.')
    parser.add_argument('--ignore', type=str, default='', help='Path patterns to ignore')

    result = parser.parse_args()
    result.ignore = result.ignore.split(',')

    return result    

def main(sourceDirs, *, command=None, destinationPath=None, fileExtensions, ignore=[]):
    monitor = FileMonitor(sourceDirs, ignore)
    logging.info('Found {} files.'.format(len(monitor.getFilesInDir())))

    destBySource = dict()
    i = 0

    for s in sourceDirs:
        destBySource[s] = destinationPath[i]
        i += 1

    def onChange(changedFile):
        if command is not None:
            subprocess.Popen(command.replace('[[file]]', changedFile).split())

        source = None

        for s in sourceDirs:
            if changedFile.startswith(s) or realpath(changedFile).startswith(realpath(s)):
                source = s
                break
        try:
            if destinationPath is not None:
                destination = destBySource[source]
                newPath = changedFile.replace(source, destination)
                newPath = realpath(newPath).replace(realpath(source), destination)

                slurp(changedFile, newPath, fileExtensions)
        except Exception as e:
            logging.error('{} {} {}'.format(e, changedFile, sourceDirs))
            logging.error(traceback.format_exc())

    monitor.onChange(onChange)

    while True:
        monitor.poll()

        sleep(DELAY_SECONDS)

if __name__ == '__main__':
    Config.init()
    args = parseArgs()

    logging.info(args.ignore)

    if args.fileExtensions is not None:
        fileExtensions = set(args.fileExtensions.split(','))
    else:
        fileExtensions = set(['html', 'htm', 'js', 'css'])

    args.sourcePath = args.sourcePath.split(',')
    args.sourcePath = list(map(abspath, args.sourcePath))

    if args.destinationPath is not None:
        args.destinationPath = args.destinationPath.split(',')

    main(args.sourcePath, command=args.command,
        destinationPath=args.destinationPath,
        fileExtensions=fileExtensions, ignore=args.ignore)
