from build_modules.fileMonitor import FileMonitor
from tests.RegressionTestCase import RegressionTestCase
from subprocess import Popen, PIPE
from os import makedirs
from os.path import join
from shutil import rmtree
from time import sleep

TIMEOUT_SECONDS = 5
OUT_DIR = 'tests/tmp/'

print('Test FileMonitor')
class test_wrap(RegressionTestCase):
    '''
        Test FileMonitor
    '''
    def setUp(self):
        try:
            rmtree(OUT_DIR)
        except:
            pass
        makedirs(OUT_DIR)

    def tearDown(self):
        rmtree(OUT_DIR)

    def test_getFilesInDir(self):
        monitor = FileMonitor(OUT_DIR)

        result = monitor.getFilesInDir()
        self.assertEqual(result, [OUT_DIR])

        newPath = join(OUT_DIR, 'test.txt')
        open(newPath, 'w').close()

        result = sorted(monitor.getFilesInDir())
        self.assertEqual(result, [OUT_DIR, newPath])

    def test_poll(self):
        monitor = FileMonitor(OUT_DIR)
        results = []

        def onChange(f):
            results.append(f)

        monitor.onChange(onChange)
        monitor.poll()

        self.assertEqual(results, [OUT_DIR])

        newPath = join(OUT_DIR, 'test.txt')
        open(newPath, 'w').close()

        sleep(0.01)

        monitor.poll()

        self.assertEqual(sorted(results), [OUT_DIR, newPath])
        