from tests.RegressionTestCase import RegressionTestCase
from subprocess import Popen, PIPE
from shutil import copyfile
from shutil import rmtree
from os import makedirs
from os.path import exists

TIMEOUT_SECONDS = 1
SOURCE_DIR = 'tests/testData/test_slurp'
OUT_DIR = 'tests/tmp'

print('test_slurp.py')
class test_slurp(RegressionTestCase):
    '''
        Test slurp.py
    '''
    def setUp(self):
        try:
            rmtree(OUT_DIR)
        except:
            pass
        makedirs(OUT_DIR)

    def tearDown(self):
        rmtree(OUT_DIR)

    def test_simple(self):
        command = './build_modules/slurp.py ' + SOURCE_DIR + ' ' + OUT_DIR

        proc1 = Popen(command, stdin=None, stdout=PIPE, stderr=PIPE, shell=True)
        proc2 = Popen('./build_modules/spit.py', stdin=proc1.stdout, stdout=PIPE, stderr=PIPE, shell=True)
        out, err = proc2.communicate(None, TIMEOUT_SECONDS)
        proc2.wait(TIMEOUT_SECONDS)

        self.assertEqual(out.decode(), '')
        self.assertEqual(err.decode(), '')

        destFile = OUT_DIR + '/file1.html'
        self.assertTrue(exists(destFile))

        f = open(destFile, 'r')
        outContents = f.read()
        f.close()

        f = open(SOURCE_DIR + '/' + 'file1.html')
        inContents = f.read()
        f.close()

        self.assertEqual(outContents, inContents)
