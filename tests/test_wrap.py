from tests.RegressionTestCase import RegressionTestCase
from subprocess import Popen, PIPE
from shutil import rmtree
from os import makedirs

TIMEOUT_SECONDS = 5
OUT_DIR = 'tests/tmp'

print('Test wrap.py')
class test_wrap(RegressionTestCase):
    '''
        Test wrap.py
    '''
    def setUp(self):
        try:
            rmtree(OUT_DIR)
        except:
            pass
        makedirs(OUT_DIR)

    def tearDown(self):
        rmtree(OUT_DIR)

    def test_simple(self):
        command1 = './build_modules/slurp.py tests/testData/test_wrap ' + OUT_DIR
        command2 = './build_modules/wrap.py \"echo testing [file]\" --fileExtensions=py'

        proc1 = Popen(command1, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=True)
        proc2 = Popen(command2, stdin=proc1.stdout, stdout=PIPE, stderr=PIPE, shell=True)
        out, err = proc2.communicate()

        proc1.wait(TIMEOUT_SECONDS)
        proc2.wait(TIMEOUT_SECONDS)

        self.assertEqual(err.decode('utf-8'), '')
        self.assertEqual(out.decode('utf-8').strip().split('\n'),
            ['tests/testData/test_wrap/file1.html', 'tests/tmp/file1.html', '1', '<h1>Test wrap.py</h1>'])
