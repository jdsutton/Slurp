from tests.RegressionTestCase import RegressionTestCase
from subprocess import Popen, PIPE
from shutil import rmtree
from os import makedirs
from os.path import dirname, exists

TIMEOUT_SECONDS = 5
OUT_DIR = 'tests/tmp'

# def makeDirsForFile(filepath):
#     dirs = dirname(filepath)
#     if not exists(dirs):
#         makedirs(dirs)

class test_fileLinker(RegressionTestCase):
    '''
        Test fileLinker.py
    '''

    def setUp(self):
        try:
            rmtree(OUT_DIR)
        except:
            pass
        makedirs(OUT_DIR)

    def tearDown(self):
        rmtree(OUT_DIR)

    def test_simple(self):
        command1 = './build_modules/slurp.py tests/testData/test_fileLinker tests/tmp'
        command2 = './build_modules/fileLinker.py --fileExtensions=css,html'
        command3 = './build_modules/spit.py'

        proc1 = Popen(command1, stdin=None, stdout=PIPE, shell=True)
        proc2 = Popen(command2, stdin=proc1.stdout, stdout=PIPE, shell=True)
        proc3 = Popen(command3, stdin=proc2.stdout, stdout=PIPE, shell=True)
        proc3.communicate()

        proc1.wait(TIMEOUT_SECONDS)
        proc2.wait(TIMEOUT_SECONDS)
        proc3.wait(TIMEOUT_SECONDS)

        self.regressionTest(
            'tests/testData/test_fileLinker/testSimpleOutput', 'tests/tmp/file1.html')
        self.regressionTest(
            'tests/testData/test_fileLinker/testDifferentFileTypesOutput', 'tests/tmp/file1.css')
        self.regressionTest(
            'tests/testData/test_fileLinker/testRelativeImports', 'tests/tmp/test_dir/file3.html')
        # self.regressionTest(
        #     'tests/testData/test_fileLinker/test_html_import_output', 'tests/tmp/html_import_syntax.html')