from tests.RegressionTestCase import RegressionTestCase
from subprocess import Popen, PIPE
from shutil import rmtree
from os import makedirs, listdir

TIMEOUT_SECONDS = 5
OUT_DIR = 'tests/tmp'

class test_cacheBuster(RegressionTestCase):
    '''
    Test cacheBuster.py
    '''

    def setUp(self):
        try:
            rmtree(OUT_DIR)
        except:
            pass
        makedirs(OUT_DIR)

    def tearDown(self):
        rmtree(OUT_DIR)

    def test_simple(self):
        command1 = './build_modules/slurp.py tests/testData/test_cacheBuster ' + OUT_DIR
        command2 = './build_modules/cacheBuster.py --fileExtensions=css,js'
        command3 = './build_modules/spit.py'

        proc1 = Popen(command1, stdin=None, stdout=PIPE, shell=True)
        proc2 = Popen(command2, stdin=proc1.stdout, stdout=PIPE, shell=True)
        proc3 = Popen(command3, stdin=proc2.stdout, stdout=PIPE, shell=True)

        proc3.communicate()

        proc1.wait(TIMEOUT_SECONDS)
        proc2.wait(TIMEOUT_SECONDS)
        proc3.wait(TIMEOUT_SECONDS)

        self.regressionTest(
            './tests/testData/test_cacheBuster/TestSimpleOutput', './tests/tmp/testSimpleInput.html')
