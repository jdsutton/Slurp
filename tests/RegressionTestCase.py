from unittest import TestCase

class RegressionTestCase(TestCase):
    '''
        Used for regression tests. Allows to compare files and update test data if desired.
    '''

    def regressionTest(self, referenceFile, outputFile):
        f = open(outputFile, 'r')
        actualText = f.read()
        f.close()

        try:
            f = open(referenceFile, 'r')
            referenceText = f.read()
            f.close()
        except FileNotFoundError:
            print('File not found: ' + referenceFile)
            print('Actual output:\n')
            print(actualText)
            print('\nUse this as new test data? (y/n)')
            response = input()

            if response in ('y', 'Y'):
                f = open(referenceFile, 'w')
                f.write(actualText)
                f.close()
                return

            self.fail('No valid test data found.')
            return

        # Compare files
        if actualText != referenceText:
            print('Expected:\n')
            print(referenceText)
            print('\nActual output:\n')
            print(actualText)
            print('\nUse this as new test data? (y/N)')
            response = input()

            if response in ('y', 'Y'):
                f = open(referenceFile, 'w')
                f.write(actualText)
                f.close()
                return

        self.assertEqual(actualText, referenceText)

