from os import getcwd
from os.path import dirname, join
from parsers.BasicImportPathParser import BasicImportPathParser
from tempfile import NamedTemporaryFile
from slurp.SlurpItem import SlurpItem
import unittest

class test_BasicImportPathParser(unittest.TestCase):
    def test_cleanPath(self):
        actual = BasicImportPathParser._cleanPath('__top__/src/JSUtils/dom.js', '/abc')
        self.assertEqual(actual, getcwd() + '/src/JSUtils/dom.js')

        actual = BasicImportPathParser._cleanPath('/src/JSUtils/dom.js', '/abc')
        self.assertEqual(actual, getcwd() + '/src/JSUtils/dom.js')

        actual = BasicImportPathParser._cleanPath('src/JSUtils/dom.js', '/abc')
        self.assertEqual(actual, '/abc/src/JSUtils/dom.js')

    def test_findDependencyPaths(self):
        f = NamedTemporaryFile(mode='w', delete=False)
        sourceFilePath = f.name
        f.write('''
            [[__top__/src/js/a.js]]
            [[/src/js/b.js]]
            [[js/c.js]]
        ''')
        f.close()

        paths = BasicImportPathParser.findDependencyPaths(sourceFilePath)
        self.assertEqual(len(paths), 3)

        self.assertEqual(paths[0].text, join(getcwd(), 'src/js/a.js'))
        self.assertEqual(paths[1].text, join(getcwd(), 'src/js/b.js'))
        self.assertEqual(paths[2].text, join(dirname(sourceFilePath), 'js/c.js'))

        f = NamedTemporaryFile(mode='w', delete=False)
        sourceFilePath = f.name
        f.write('''
            .TestClass {
                margin: auto;
            }
        ''')
        f.close()

        paths = BasicImportPathParser.findDependencyPaths(sourceFilePath)
        self.assertEqual(len(paths), 0)

    def test_getNextMatch(self):
        contents = '''
            [[__top__/src/js/a.js]]
            [[js/c.js]]
        '''

        item = SlurpItem('a/b/c/d', None, contents)

        # Assert input order.
        path = BasicImportPathParser.getNextMatch(item)
        self.assertEqual(path.text, join(getcwd(), 'src/js/a.js'))

if __name__ == '__main__':
    unittest.main()