from os import getcwd
from os.path import dirname, join
from parsers.HTMLImportPathParser import HTMLImportPathParser
from tempfile import NamedTemporaryFile
import unittest

class test_HTMLImportPathParser(unittest.TestCase):

    def test_findDependencyPaths(self):
        f = NamedTemporaryFile(mode='w', delete=False)
        sourceFilePath = f.name
        f.write('''
            <import path="/src/js/a.js">
            <import path="js/b.js">
        ''')
        f.close()

        paths = HTMLImportPathParser.findDependencyPaths(sourceFilePath)
        self.assertEqual(len(paths), 2)

        self.assertEqual(paths[0].text, join(getcwd(), 'src/js/a.js'))
        self.assertEqual(paths[1].text, join(dirname(sourceFilePath), 'js/b.js'))

        f = NamedTemporaryFile(mode='w', delete=False)
        sourceFilePath = f.name
        f.write('''
            .TestClass {
                margin: auto;
            }
        ''')
        f.close()

        paths = HTMLImportPathParser.findDependencyPaths(sourceFilePath)
        self.assertEqual(len(paths), 0)

if __name__ == '__main__':
    unittest.main()