from os import getcwd
from os.path import dirname, join
from parsers.MetaImportPathParser import MetaImportPathParser
from slurp.SlurpItem import SlurpItem
from tempfile import NamedTemporaryFile
import unittest

class test_MetaImportPathParser(unittest.TestCase):

    def test_getNextMatch(self):
        contents = '''[[__top__/src/js/a.js]]
            <import path="/src/js/b.js">
            [[js/c.js]]
            import("/src/js/d.js");'''

        item = SlurpItem('a/b/c/d.js', None, contents)

        # Assert input order.
        path = MetaImportPathParser.getNextMatch(item).text
        self.assertEqual(path, join(getcwd(), 'src/js/a.js'))

        item.contents = '\n'.join(item.contents.split('\n')[1:])
        path = MetaImportPathParser.getNextMatch(item).text
        self.assertEqual(path, join(getcwd(), 'src/js/b.js'))
        
        item.contents = '\n'.join(item.contents.split('\n')[1:])
        path = MetaImportPathParser.getNextMatch(item).text
        self.assertEqual(path, join(getcwd(), 'a/b/c/js/c.js'))

        item.contents = '\n'.join(item.contents.split('\n')[1:])
        path = MetaImportPathParser.getNextMatch(item).text
        self.assertEqual(path, join(getcwd(), 'src/js/d.js'))

if __name__ == '__main__':
    unittest.main()