#! /usr/bin/env python3

from parsers.MetaImportPathParser import MetaImportPathParser
from slurp.SlurpItem import SlurpItem
import unittest

class test_MetaImportPathParser(unittest.TestCase):

    def test_handleItem(self):      
        code = '''
            <script src="driver.js"></script>
        '''
        expected = '''
            <script src="driver.js"></script>
        '''

        item = SlurpItem('a/b/c', 'd/e/f', code)

        MetaImportPathParser.handleItem(item)

        result = item.contents

        self.assertEqual(result, expected)

if __name__ == '__main__':
    unittest.main()