from dependency.DependencyAnalyzer import DependencyAnalyzer
from os import getcwd
from os.path import join
from tempfile import NamedTemporaryFile
import unittest

class test_DependencyAnalyzer(unittest.TestCase):

    def test_findDependencyPaths(self):
        f = NamedTemporaryFile(mode='w', delete=False)
        sourceFilePath = f.name
        cwd = getcwd()
        f.write('''
            <import path="/dependency/test/d1.js">
            [[imports /dependency/test/d2.js]]
            import('/dependency/test/d4.js');
            <script src='/dependency/test/d5.js'></script>
        ''')
        f.close()

        otherPath = join(cwd, 'dependency/test/d1.js')
        self.assertTrue(DependencyAnalyzer.fileDependsOn(sourceFilePath, otherPath))
        otherPath = join(cwd, 'dependency/test/d2.js')
        self.assertTrue(DependencyAnalyzer.fileDependsOn(sourceFilePath, otherPath))
        otherPath = join(cwd, 'dependency/test/d4.js')
        self.assertTrue(DependencyAnalyzer.fileDependsOn(sourceFilePath, otherPath))
        otherPath = join(cwd, 'dependency/test/d5.js')
        self.assertTrue(DependencyAnalyzer.fileDependsOn(sourceFilePath, otherPath))

        otherPath = join(cwd, 'dependency/test/d3.js')
        self.assertFalse(DependencyAnalyzer.fileDependsOn(sourceFilePath, otherPath))

if __name__ == '__main__':
    unittest.main()