from os.path import realpath, dirname, isfile, samefile
from parsers.MetaImportPathParser import MetaImportPathParser

class DependencyAnalyzer:

    @staticmethod
    def fileDependsOn(fileA, fileB):
        '''
        Returns True if fileA depends on fileB.
        Accepts paths as arguments.
        '''
        
        fileA = realpath(fileA)
        fileB = realpath(fileB)

        matches = MetaImportPathParser.findDependencyPaths(fileA)

        for match in matches:
            if isfile(match.text) and isfile(fileB) and samefile(match.text, fileB):
                return True

        return False

    @staticmethod
    def getFilesDependingOn(pathList, filePath):
        '''
        Returns a list of files from pathList which depend directly on filePath.
        '''

        filePath = realpath(filePath)
        results = set()

        for path in pathList:
            path = realpath(path)

            if not isfile(path):
                continue

            if DependencyAnalyzer.fileDependsOn(path, filePath):
                results.add(path)
                
        return results