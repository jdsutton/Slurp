from os.path import realpath
from parsers.ImportPathParser import ImportPathParser

class ImportsFlagPathParser(ImportPathParser):
    PATTERN = '\[\[imports [\.a-zA-Z0-9_/]+\.[a-zA-Z]+\]\]'
    INJECT_FILE_CONTENTS = False

    @classmethod
    def _cleanPatternMatch(cls, text, sourcePath):
        return realpath(cls._cleanPath(text[10:-2], sourcePath))
