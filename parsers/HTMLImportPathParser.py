from os.path import realpath
from parsers.ImportPathParser import ImportPathParser

class HTMLImportPathParser(ImportPathParser):
    PATTERN = '<import path="[\.a-zA-Z0-9\-_/]+\.[a-zA-Z]+">'

    @classmethod
    def _cleanPatternMatch(cls, text, sourcePath):
        return realpath(cls._cleanPath(text[14:-2], sourcePath))
