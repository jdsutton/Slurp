from os.path import realpath
from parsers.ImportPathParser import ImportPathParser

class JavaScriptImportPathParser(ImportPathParser):
    PATTERN = 'import\([\'"][\.a-zA-Z0-9\-_/]+\.[a-zA-Z]+[\'"]\);?'

    @classmethod
    def _cleanPatternMatch(cls, text, sourcePath):
        return realpath(cls._cleanPath(text[8:-3], sourcePath))
