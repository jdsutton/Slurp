from parsers.BasicImportPathParser import BasicImportPathParser
from parsers.HTMLImportPathParser import HTMLImportPathParser
from parsers.HTMLSrcImportPathParser import HTMLSrcImportPathParser
from parsers.ImportPathParser import ImportPathParser
from parsers.ImportsFlagPathParser import ImportsFlagPathParser
from parsers.JavaScriptImportPathParser import JavaScriptImportPathParser
from py3typing.Typing import *
from slurp.SlurpItem import SlurpItem

class MetaImportPathParser(ImportPathParser):

    @classmethod
    def _getNextMatchWithParser(cls, item, startIndex, *, extraParsers=[]):
        parsers = [
            ImportsFlagPathParser,
            HTMLImportPathParser,
            HTMLSrcImportPathParser,
            BasicImportPathParser,
            JavaScriptImportPathParser,
        ] + extraParsers

        matches = [[parser.getNextMatch(item, startIndex), parser] for parser in parsers]
        matches = [match for match in matches if match[0] is not None]

        if len(matches) == 0:
            return [None, None]

        matches.sort(key=lambda match: match[0].start)

        return matches[0]

    @classmethod
    def getNextMatch(cls, item, startIndex=0, extraParsers=[]):
        '''
        Returns the next ImportPathParser.Match in the given SlurpItem, in order.
        Else returns None.
        '''

        if item is None:
            return None

        match, _ = cls._getNextMatchWithParser(item, startIndex, extraParsers=extraParsers)

        return match

    @classmethod
    def findDependencyPaths(cls, filePath, * , extraParsers=[]):
        '''
        '''

        matches = []
        startIndex = 0
        item = SlurpItem.fromPath(filePath)
        result = cls.getNextMatch(item, startIndex, extraParsers=extraParsers)

        if item is None:
            return []

        while result is not None:
            matches.append(result)
            startIndex = max(result.end, startIndex + 1)
        
            result = cls.getNextMatch(item, startIndex, extraParsers=extraParsers)

        return matches

        
    @classmethod
    def _replaceTagsWithFileContents(cls, item, filesAlreadyImported):
        i = 0
        match, parser = cls._getNextMatchWithParser(item, i)
        
        while match is not None:
            i = match.start + 1

            parser._replaceTagWithFileContents(item, match, filesAlreadyImported, parser=cls)
            
            match, parser = cls._getNextMatchWithParser(item, i)
