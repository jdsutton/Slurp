from os.path import realpath
from parsers.ImportPathParser import ImportPathParser

class BasicImportPathParser(ImportPathParser):
    PATTERN = '\'?\[\[[\.a-zA-Z0-9_/]+\.[a-zA-Z]+\]\]\'?'

    @classmethod
    def _cleanPatternMatch(cls, text, sourcePath):
        result = text.strip('\'').strip('[').strip(']')
        result = cls._cleanPath(result, sourcePath)

        return realpath(result)