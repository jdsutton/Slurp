#! /usr/bin/env python3

from parsers.ScriptTagIterator import ScriptTagIterator
import unittest

class test_ScriptTagIterator(unittest.TestCase):

    def test_iterate(self):
        js = '''
            HTMLString._PATTERN = /[&<>"'\/]/g;
            JSRender._pattern = /\[\[[a-zA-Z0-9_ ]+\]\]/;

            const x = '\'';

            /**
             * @class
             */
            class MyClass {
                static async foo(a, b, c, d) {
                    return a < b > c > d; // Lol
                }
            }
        '''
        code = '''
            <!DOCTYPE HTML>
            <html lang="en">
                <head>
                    <link rel="stylesheet" type="text/css" href="style.css">
                    <title>Analytics</title>
                    <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
                </head>

                <body class="ViewContainer">
                    <div></div>
                    <div id="container"></div>
                </body>

                <script>{}</script>

                <!-- <script>Ignore Me</script> -->

                <script data-blah=">">// Get me.</script>
                <script>// Me too.</script>
            </html>
        '''.format(js)

        expected = [
            '',
            js,
            '// Get me.',
            '// Me too.',
        ]

        iterator = ScriptTagIterator(code)

        scripts = list(iterator)

        self.assertEqual(len(scripts), len(expected))

        i = 0
        for script in scripts:
            self.assertEqual(script.contents, expected[i])

            i += 1

        self.assertEqual(iterator.contents, code)

    def test_replaceScripts(self):
        js = '''
            <script>{foo}</script>
            <script>{foo}</script>
            <script>{foo}</script>
        '''

        iterator = ScriptTagIterator(js)

        for script in iterator:
            iterator.replaceScript(script, 'foo')

        self.assertEqual(iterator.contents, js.format(foo='foo'))

if __name__ == '__main__':
    unittest.main()