from os import getcwd
from os.path import realpath, dirname, join, sep as pathSeparator
import logging
from py3typing.Typing import *
from re import finditer, search
from slurp.SlurpItem import SlurpItem

logging.basicConfig(filename='.slurplog', filemode='w', level=logging.DEBUG)

class ImportPathParser:
    PATTERN = None
    INJECT_FILE_CONTENTS = True
    DELETE_PATTERN = True

    class Match:
        def __init__(self, text, start, end):
            self.text = text
            self.start = start
            self.end = end

        def __str__(self):
            return '{} [{}:{}]'.format(self.text, self.start, self.end)

    class MatchIterator:
        def __init__(self, parserClass, item):
            self._parserClass
            self._item = item
            self._i = 0

        def __iter__(self):
            return self

        def __next__(self):
            nextMatch = self._parserClass.getNextMatch(self._item, self._i)

            if nextMatch is None:
                raise StopIteration()

            self._i = nextMatch.start + 1

            return nextMatch

    @classmethod
    def getNextMatch(cls, item, startIndex=0):
        sourceDir = item.getSourceDirectory()
        text = item.contents[startIndex:]

        result = search(cls.PATTERN, text)
        
        if result is None:
            return None

        text = cls._cleanPatternMatch(result.group(), sourceDir)

        return ImportPathParser.Match(text, startIndex + result.start(), startIndex + result.end())

    @classmethod
    def _cleanPath(cls, path, sourceDir):
        if path.startswith(pathSeparator) or path.startswith('__top__'):
            path = path.replace('__top__', '')
            return join(getcwd(), path[1:])
        else:   
            return join(sourceDir, path)

    @classmethod
    def _cleanPatternMatch(cls, text, sourcePath):
        return realpath(cls._cleanPath(text[2:-2], sourcePath))

    @classmethod
    def findDependencyPathsInText(cls, contents, sourcePath):
        results = list(finditer(cls.PATTERN, contents))

        for i in range(len(results)):
            text = cls._cleanPatternMatch(results[i].group(), sourcePath)
            results[i] = ImportPathParser.Match(text, results[i].start(), results[i].end())

        return results

    @classmethod
    def findDependencyPaths(cls, filePath):
        sourcePath = dirname(realpath(filePath))

        try:
            with open(filePath, 'r') as f:
                contents = f.read()
        except UnicodeDecodeError:
            return []

        return cls.findDependencyPathsInText(contents, sourcePath)

    @classmethod
    def _replaceTagWithFileContents(cls, item, match, filesAlreadyImported, *, parser=None):
        text = item.contents
        fileName = match.text

        if parser is None:
            parser = cls

        if not realpath(fileName) in filesAlreadyImported:
            filesAlreadyImported.add(realpath(fileName))
            
            try:
                with open(fileName, 'r') as f:
                    fileText = f.read()

                dependency = SlurpItem(fileName, item.dest, fileText)

                parser.handleItem(dependency, filesAlreadyImported)

                fileText = dependency.contents
            except Exception as e:
                fileText = str(e)

            # Replace with file contents.
            inner = ''
            if cls.INJECT_FILE_CONTENTS:
                inner = fileText

            if cls.DELETE_PATTERN:
                text = text[:match.start] + inner + text[match.end:]

        elif cls.DELETE_PATTERN:
            text = text[:match.start] + text[match.end:]

        item.contents = text

    @classmethod
    def _replaceTagsWithFileContents(cls, item, filesAlreadyImported):
        for result in ImportPathParser.MatchIterator(cls, item):
            cls._replaceTagWithFileContents(item, result, filesAlreadyImported)
            result = cls.getNextMatch(item)

    @classmethod
    @Params(object, SlurpItem, set)
    def handleItem(cls, item, filesAlreadyImported=None):
        if filesAlreadyImported is None:
            filesAlreadyImported = set()

        cls._replaceTagsWithFileContents(item, filesAlreadyImported)
