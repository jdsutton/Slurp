import re

class ScriptTagIterator:

    class _OPEN_SYMBOLS:
        SCRIPT = '<script'
        SINGLE_QUOTE = '\''
        DOUBLE_QUOTE = '"'
        BACKTICK = '`'
        COMMENT = '<!--'

    _ALL_OPEN_SYMBOLS = set([
        s for s in _OPEN_SYMBOLS.__dict__.values()
        if isinstance(s, str) and not s.startswith('_')
    ])

    _SCRIPT_TAG_SYMBOL = '<'

    class _CLOSE_SYMBOLS:
        SCRIPT = '</script>'
        COMMENT = '-->'
        SCRIPT_TAG = '>'
        SINGLE_QUOTE = '\''
        DOUBLE_QUOTE = '"'
        BACKTICK = '`'

    _CLOSE_SYMBOLS_BY_OPEN_SYMBOL = dict()
    _CLOSE_SYMBOLS_BY_OPEN_SYMBOL[_SCRIPT_TAG_SYMBOL] = _CLOSE_SYMBOLS.SCRIPT_TAG

    for key in dir(_OPEN_SYMBOLS):
        if key.startswith('__'): continue

        _CLOSE_SYMBOLS_BY_OPEN_SYMBOL[getattr(_OPEN_SYMBOLS, key)] = \
            getattr(_CLOSE_SYMBOLS, key)

    _SYMBOL_PATTERNS = [
        v for k, v in
        list(_OPEN_SYMBOLS.__dict__.items()) + list(_CLOSE_SYMBOLS.__dict__.items())
        if isinstance(v, str) and not k.startswith('_')
    ]
    _ANY_SYMBOL_REGEX = re.compile('|'.join(_SYMBOL_PATTERNS))

    class Script:
        def __init__(self, indexInFile, contents):
            self.indexInFile = indexInFile
            self.contents = contents

    def __init__(self, code):
        self._openSymbolStack = []
        self._i = 0
        self._fileContents = code
        self._prevOpenScriptTagEndIndex = 0
        self._lastClosedSymbol = None

    def __iter__(self):
        return self

    def _getOpenBlockSymbol(self):
        if len(self._openSymbolStack) > 0:
            return self._openSymbolStack[-1]

        return None

    def _getCloseSymbol(self, openSymbol):
        return ScriptTagIterator._CLOSE_SYMBOLS_BY_OPEN_SYMBOL[openSymbol]

    def _proceedToNextBlockSymbol(self, pattern):
        if isinstance(pattern, str):
            pattern = re.compile(pattern)

        match = pattern.search(self._fileContents, self._i)

        if match is None:
            raise StopIteration()
        
        self._i = match.end()

        return match.group(0)

    def _symbolClosesCurrentBlock(self, symbol):
        openSymbol = self._getOpenBlockSymbol()

        if openSymbol is None:
            return False

        return symbol == self._getCloseSymbol(openSymbol)

    def _insideJSContext(self):
        return self._lastClosedSymbol == ScriptTagIterator._SCRIPT_TAG_SYMBOL

    def __next__(self):
        while True:
            if self._insideJSContext():
                # Aggressively search for close script symbol.
                # TODO: Parse JS watching for escape symbols.
                symbol = self._proceedToNextBlockSymbol(ScriptTagIterator._CLOSE_SYMBOLS.SCRIPT)
            else:
                symbol = self._proceedToNextBlockSymbol(ScriptTagIterator._ANY_SYMBOL_REGEX)

            if self._symbolClosesCurrentBlock(symbol):
                # Close block.
                self._lastClosedSymbol = self._openSymbolStack.pop()

                if symbol == ScriptTagIterator._CLOSE_SYMBOLS.SCRIPT_TAG:
                    # Remember where script tag started.
                    self._prevOpenScriptTagEndIndex = self._i
                elif len(self._openSymbolStack) == 0 and symbol == ScriptTagIterator._CLOSE_SYMBOLS.SCRIPT:
                    # If that was a script, grab contents.
                    startI = self._prevOpenScriptTagEndIndex
                    script = self._fileContents[startI:self._i - len(symbol)]

                    return ScriptTagIterator.Script(startI, script)
            elif symbol in ScriptTagIterator._ALL_OPEN_SYMBOLS:
                # Open block.
                self._openSymbolStack.append(symbol)

                if symbol == ScriptTagIterator._OPEN_SYMBOLS.SCRIPT:
                    self._openSymbolStack.append(ScriptTagIterator._SCRIPT_TAG_SYMBOL)

    @property
    def contents(self):
        return self._fileContents
    
    def replaceScript(self, script, value):
        endIndex = script.indexInFile + len(script.contents)

        self._fileContents = self._fileContents[:script.indexInFile] + \
            value + self._fileContents[endIndex:]

        self._i = script.indexInFile + len(value)
