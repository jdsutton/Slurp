from os.path import realpath
from parsers.ImportPathParser import ImportPathParser

class HTMLSrcImportPathParser(ImportPathParser):
    PATTERN = '<script src=[\'"][\.a-zA-Z0-9\-_/]+\.[a-zA-Z]+[\'"]></script>'
    INJECT_FILE_CONTENTS = False
    DELETE_PATTERN = False

    @classmethod
    def _cleanPatternMatch(cls, text, sourcePath):
        return realpath(cls._cleanPath(text[13:-11], sourcePath))
