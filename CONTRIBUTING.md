# Contributing

Contributions to this project are welcome for all. Please do not work in the master branch. Tests are strongly encouraged.


## Beginners
Try clicking on the "issues" or "code climate" shields in the readme. They will list some small things you can fix.
