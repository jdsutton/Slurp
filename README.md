# Slurp
[![Build Status](https://travis-ci.org/jdsutton/Slurp.svg?branch=master)](https://travis-ci.org/jdsutton/Slurp)
[![Code Climate](https://codeclimate.com/github/jdsutton/Slurp/badges/gpa.svg)](https://codeclimate.com/github/jdsutton/Slurp)
[![Python Version](https://img.shields.io/badge/Python-3-brightgreen.svg?style=plastic)](http://python.org)

A language-agnostic, modular set of tools for real-time builds.

:star: If you use this software, please [contribute](https://github.com/jdsutton/Slurp/blob/master/CONTRIBUTING.md) and consider [donating](https://github.com/jdsutton/Technical-Interview-Megarepo/blob/master/DONATE.md). Thank you. :star:

## Features
* Usage requires only basic knowledge of the UNIX terminal and Makefiles.
* Modular build process.
* Modules can be written in any language.
* Intelligent, continuous, realtime build process.
* Generic support for integrating arbitrary executables.

## Installation
```
$ cd path/to/my/project
$ git submodule add https://github.com/jdsutton/Slurp.git
$ git submodule update --init --recursive
```

## Usage
See [Example_Build_Makefile](https://github.com/jdsutton/Slurp/blob/master/Example_Build_Makefile)

**Example 0: Basic file linking**

*Makefile:*
```
build:
	BUILD=Slurp/slurp/build

	@$(BUILD)/slurp.py ./src ./dist \
	| $(BUILD)/fileLinker.py --fileExtensions=html,js \
	| $(BUILD)/spit.py
```

**Example 1: Run continous build**

*Makefile:*
```
build_continuous:
	python3 $(BUILD)/fileMonitor.py $(SRC) --destinationPath=$(DIST) --ignore=static,images \
	...
	| $(BUILD)/spit.py &
	
	<Run your server here>
```

## Build module documentation
Run:

`$ make docs`

...and view the output under documentation/

## Writing your own build modules
In the language of your choice:

0. Implement SlurpItem.py in your language of choice if needed.
1. Call SlurpItem.readIn()
2. Apply some operation (eg: compress, rename, etc.)
3. Call .print() on the SlurpItem
4. Repeat until EOF

Then add your executable to the build:
`... | myExecutableModule.idk ....`
